package com.epam.stage2;

import java.util.Scanner;

public class UtilityController {
    private final View view;
    private final Scanner scanner;

    public UtilityController(Scanner scanner, View view) {
        this.view = view;
        this.scanner = scanner;
    }

    String inputValue(String s, String regex) {
        view.printMessage(s);
        String input;
        while (!(scanner.hasNext() && (input = scanner.next()).matches(regex))) {
                view.printMessage("Wrong input");
        }
        //scanner.nextLine();
        return input;
    }
}
