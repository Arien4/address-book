package com.epam.stage2;

import com.epam.stage2.model.Model;

import java.util.Scanner;

public class Controller {
    private final Model model;
    private final View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    public void run() {
        Scanner scanner = new Scanner(System.in);
        Record record = new Record(view, scanner);
        record.inputRecord();

    }

    private Record getRecord() {
        return null;
    }
}
