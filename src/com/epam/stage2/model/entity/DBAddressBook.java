package com.epam.stage2;

public enum DBAddressBook {
    RECORD_ONE("Oleh", "oleh1993"),
    RECORD_TWO("Olha", "olhavinnik"),
    RECORD_THREE("Val", "Valery_2021");

    private final String firstName;
    private final String login;

    DBAddressBook(String firstName, String login) {
        this.firstName = firstName;
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLogin() {
        return login;
    }

    public static boolean isLoginAlreadyInUse(String loginData) {
        for (DBAddressBook record: DBAddressBook.values()) {
            if (record.getLogin().equals(loginData)) {
                return true;
            }
        }
        return false;
    }
}
